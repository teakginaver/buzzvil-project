package com.example.myapptest.utils.localdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.myapptest.utils.localdb.dao.CampaignsDao;
import com.example.myapptest.utils.localdb.dao.ConfigDao;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;

@Database(entities = {
        CampaignsEntity.class,
        ConfigEntity.class
},version = 1,exportSchema = false)
public abstract class LocalDatabase extends RoomDatabase {
    public abstract CampaignsDao getCampaignsDao();
    public abstract ConfigDao getConfigDao();
}
