package com.example.myapptest.utils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


/**
 *
 * 첫페이지에 무엇을 보여줄지 결정
 *
 * */
public final class CampaignsSorter {
    private ConfigEntity config;
    private List<CampaignsEntity> entities;

    public CampaignsSorter(ConfigEntity config, List<CampaignsEntity> entities) {
        this.config = config;
        this.entities = entities;
    }

    public List<PageItem> start() {
        insertFirstpage(entities, config);//광고 와 컨텐츠중 첫번째 페이지 선정

        List<PageItem> list = new ArrayList<>();
        for (CampaignsEntity entity : entities){
            list.add(new PageItem(entity));
        }
        return list;
    }


    /**
     * 광고 와 컨텐츠중 첫번째 페이지 선택
     * */
    private void insertFirstpage(@NonNull List<CampaignsEntity> entitys, @NonNull ConfigEntity config) {
        final Category category = getCategoryByConfig(config);
        final int firstDisplayPriority = getFirstDisplayPriority(category, entitys);

        if(firstDisplayPriority < 0){//오류 발생시. 랜덤 섞기만.
            setFirstpageAndSortRandom(null , entitys);
            return;
        }
        //가중치 적용전 FirstDisplayPriority 가 같은 캠페인 리스트
        List<CampaignsEntity> firstpageList = getFirstpageListByPriority(entitys, category, firstDisplayPriority);
        //후보리스트중 가중치 적용해서 하나만 뽑기 -> 첫페이지 구하기 끝.
        CampaignsEntity firstPage = selectOnepage(firstpageList);
        //랜덤 섞기 후 첫페이지 적용
        setFirstpageAndSortRandom(firstPage ,entitys);
    }


    /**
     *  AD, CONTENTS 중 선택
     * */
    private Category getCategoryByConfig(@NonNull ConfigEntity config){
        try{
            int allCnt = config.allCnt();
            int currentIdx = config.getFirstViewCnt() % allCnt;
            if(currentIdx < config.getAdCnt())return Category.AD;
        }catch(Exception e){e.printStackTrace();}
        return Category.CONTENTS;
    }


    /**
     * 모든 캠페인을 섞고, 첫번째 페이지 셋팅
     * */
    private void setFirstpageAndSortRandom(@Nullable CampaignsEntity firstPage, @NonNull List<CampaignsEntity> entitys) {
        Collections.shuffle(entitys);
        if(entitys.contains(firstPage)){
            entitys.remove(firstPage);
            entitys.add(0, firstPage);
        }
    }


    /**
     * 첫번째 Entity 찾기 - AD or Contents 중에서 우선순위가 가장 낮은값 리턴
     * */
    private int getFirstDisplayPriority(@NonNull Category category, @NonNull List<CampaignsEntity> entitys) {
        for (CampaignsEntity entity : entitys) {
            if(entity.getCategory() == category)return entity.getFirstDisplayPriority();
        }
        return -1;
    }

    /**
     * firstDisplayPriority 같은 entity 모두 검색
     * */
    private List<CampaignsEntity> getFirstpageListByPriority(List<CampaignsEntity> entitys, Category category, int firstDisplayPriority) {
        List<CampaignsEntity> list = new ArrayList<>();
        for (CampaignsEntity entity : entitys) {
            if(entity.getCategory() == category && entity.getFirstDisplayPriority() > firstDisplayPriority)break;
            if(entity.getCategory() == category && entity.getFirstDisplayPriority() == firstDisplayPriority)
                list.add(entity);
        }
        return list;
    }


    /**
     * 후보중 하나를 선택
     * */
    private CampaignsEntity selectOnepage(List<CampaignsEntity> firstpagelist){
        if(firstpagelist.size() == 0)return null;
        int sumFirstDisplayWeight = 0;
        for (CampaignsEntity entity : firstpagelist)
            sumFirstDisplayWeight += entity.getFirstDisplayWeight();

        int idx = new Random().nextInt(sumFirstDisplayWeight) + 1;
        for (CampaignsEntity entity : firstpagelist) {
            idx -= entity.getFirstDisplayWeight();
            if(idx <= 0)return entity;
        }

        return firstpagelist.get(firstpagelist.size() - 1);
    }

}
