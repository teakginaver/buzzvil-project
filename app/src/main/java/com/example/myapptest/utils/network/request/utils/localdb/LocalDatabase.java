package com.example.myapptest.utils.network.request.utils.localdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.myapptest.utils.network.request.utils.localdb.dao.AdDao;
import com.example.myapptest.utils.network.request.utils.localdb.dao.ConfigDao;
import com.example.myapptest.utils.network.request.utils.localdb.entity.AdEntity;
import com.example.myapptest.utils.network.request.utils.localdb.entity.ConfigEntity;

@Database(entities = {
        AdEntity.class,
        ConfigEntity.class
},version = 1)
public abstract class LocalDatabase extends RoomDatabase {
    public abstract AdDao getAdDao();
    public abstract ConfigDao getConfigDao();
}
