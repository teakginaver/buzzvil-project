package com.example.myapptest.utils.localdb.entity;

import androidx.annotation.NonNull;

public class CampaignsSubInfo {
    private int id;
    private String name;
    private String imageUrl;
    private int firstDisplayPriority;
    private int firstDisplayWeight;
    private int frequency;
    private String landing_url;

    private long local_datetime;
    public CampaignsSubInfo() {}

    public CampaignsSubInfo(@NonNull CampaignsEntity entity) {
        this.id = entity.getId();
        this.name = entity.getName();
        this.imageUrl = entity.getImageUrl();
        this.firstDisplayPriority = entity.getFirstDisplayPriority();
        this.firstDisplayWeight = entity.getFirstDisplayWeight();
        this.frequency = entity.getFrequency();
        this.landing_url = entity.getLanding_url();
        this.local_datetime = entity.getLocal_datetime();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getFirstDisplayPriority() {
        return firstDisplayPriority;
    }

    public int getFirstDisplayWeight() {
        return firstDisplayWeight;
    }

    public int getFrequency() {
        return frequency;
    }

    public String getLanding_url() {
        return landing_url;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setFirstDisplayPriority(int firstDisplayPriority) {
        this.firstDisplayPriority = firstDisplayPriority;
    }

    public void setFirstDisplayWeight(int firstDisplayWeight) {
        this.firstDisplayWeight = firstDisplayWeight;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setLanding_url(String landing_url) {
        this.landing_url = landing_url;
    }

    public long getLocal_datetime() {
        return local_datetime;
    }

    public void setLocal_datetime(long local_datetime) {
        this.local_datetime = local_datetime;
    }
}
