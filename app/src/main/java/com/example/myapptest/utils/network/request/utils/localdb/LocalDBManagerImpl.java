package com.example.myapptest.utils.network.request.utils.localdb;

import android.content.Context;
import android.util.Log;

import androidx.room.Room;

import com.example.myapptest.utils.network.request.utils.localdb.dao.AdDao;
import com.example.myapptest.utils.network.request.utils.localdb.dao.ConfigDao;
import com.example.myapptest.utils.network.request.utils.localdb.entity.AdEntity;
import com.example.myapptest.utils.network.request.utils.localdb.entity.Category;
import com.example.myapptest.utils.network.request.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.ArrayList;
import java.util.List;

public class LocalDBManagerImpl implements LocalDBManager {
    private final String DATABASE_NAME = "localdb";
    private Context context;

    private LocalDatabase db;

    public LocalDBManagerImpl(Context context) {
        this.context = context;
        db = Room.databaseBuilder(context, LocalDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();
    }

    @Override
    public void close() {
        db.close();
    }

    @Override
    public AdDao getAdDao() { return db.getAdDao();}

    @Override
    public ConfigDao getConfigDao() {return db.getConfigDao();}


    @Override
    public List<PageItem> getPageItemList() {
        ConfigEntity config = getConfigDao().getConfig();

        List<AdEntity> adList = getAdDao().getAll();

        PageItem firstPage = firstPageFindItem(adList, config);



        List<PageItem> list = new ArrayList<>();
        for (AdEntity entity : adList){
            list.add(new PageItem(entity));
        }

        return sortPageItem(config, list);
    }


    private List<PageItem> sortPageItem(ConfigEntity config, List<PageItem> list){

        return list;
    }


    private PageItem firstPageFindItem(List<AdEntity> adList, ConfigEntity config) {
        Category category = currentContents(config);

        if(category == Category.AD){

        }else if(category == Category.CONTENTS){

        }
        for (AdEntity entity : adList) {
            //Log.e("PJG","sort : " + entity.toString());
        }
        return null;

    }

    private Category currentContents(ConfigEntity config){
        try{
            int allCnt = config.allCnt();
            int currentIdx = config.getFirstViewCnt() % allCnt;
            if(currentIdx >= config.getAdCnt())return Category.AD;
        }catch(Exception e){e.printStackTrace();}
        return Category.CONTENTS;
    }
}
