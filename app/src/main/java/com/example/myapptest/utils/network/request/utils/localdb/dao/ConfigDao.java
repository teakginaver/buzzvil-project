package com.example.myapptest.utils.network.request.utils.localdb.dao;

import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapptest.utils.network.request.utils.localdb.entity.ConfigEntity;

@Dao
public abstract class ConfigDao {
    @Insert
    public abstract void insert(ConfigEntity entity);
    @Update
    public abstract void update(ConfigEntity entity);

    @Delete
    public abstract void delete(ConfigEntity entity);
    @Query("select * from config")
    public abstract ConfigEntity getConfig();

    public void insertOrUpdate(ConfigEntity entity){
        try{
            insert(entity);
        }catch (SQLiteConstraintException e){
            ConfigEntity oldConfig = getConfig();
            if(!oldConfig.getFirstAdRatio().equals(entity.getFirstAdRatio())){
                update(entity);
            }
        }
    }
}
