package com.example.myapptest.utils.network.request.utils.localdb;

import com.example.myapptest.utils.network.request.utils.localdb.dao.AdDao;
import com.example.myapptest.utils.network.request.utils.localdb.dao.ConfigDao;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.List;

public interface LocalDBManager {
    List<PageItem> getPageItemList();

    void close();

    AdDao getAdDao();
    ConfigDao getConfigDao();


}
