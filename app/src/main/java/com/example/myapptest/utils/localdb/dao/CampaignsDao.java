package com.example.myapptest.utils.localdb.dao;

import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.utils.localdb.entity.CampaignsSubInfo;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;


import org.jetbrains.annotations.NotNull;

import java.util.List;

@Dao
public interface CampaignsDao {
    @Insert
    public void insert(CampaignsEntity entity);

    @Update(entity = CampaignsEntity.class)
    public void updateSubInfo(CampaignsSubInfo subInfo);

    @Query("DELETE FROM campaigns WHERE local_datetime <> :updateTime AND category = :category")
    public void deleteByUpdateTime(long updateTime, Category category);

    @Query("DELETE FROM campaigns WHERE id = :id")
    public void deleteById(int id);

    @Query("UPDATE campaigns SET view_cnt = :viewcnt WHERE id = :id")
    public int updateViewCnt(int viewcnt, int id);

    @Query("UPDATE campaigns SET favorites = :favorites WHERE id = :id")
    public int updateFavorites(Favorites favorites, int id);

    @Query("SELECT * FROM campaigns WHERE frequency > view_cnt ORDER BY firstDisplayPriority")
    public List<CampaignsEntity> findCampaignsList();


}
