package com.example.myapptest.utils.network.request.utils.network.request;

public abstract class BaseRequest {
    private String METHOD;
    public BaseRequest(String method){
        METHOD = method;
    }
    public String getMethod() {
        return METHOD;
    }
}
