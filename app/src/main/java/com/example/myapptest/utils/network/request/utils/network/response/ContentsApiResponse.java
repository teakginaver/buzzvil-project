package com.example.myapptest.utils.network.request.utils.network.response;

import com.example.myapptest.utils.network.request.utils.localdb.entity.AdEntity;
import com.example.myapptest.utils.network.request.utils.localdb.entity.Category;

import java.util.List;
import java.util.stream.Collectors;

public class ContentsApiResponse {

    private List<AdApiResponse.Campaigns> campaigns;

    public List<AdEntity> getContentsEntity(long updateTime){
        return campaigns.stream().map(campaign -> new AdEntity(campaign, updateTime, Category.CONTENTS)).collect(Collectors.toList());
    }
}
