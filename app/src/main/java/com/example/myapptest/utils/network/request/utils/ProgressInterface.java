package com.example.myapptest.utils.network.request.utils;

public interface ProgressInterface {
    public void startProgress();
    public void endProgress();
}
