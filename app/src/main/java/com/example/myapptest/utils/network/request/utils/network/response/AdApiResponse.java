package com.example.myapptest.utils.network.request.utils.network.response;

import com.example.myapptest.utils.network.request.utils.localdb.entity.AdEntity;
import com.example.myapptest.utils.network.request.utils.localdb.entity.Category;

import java.util.List;
import java.util.stream.Collectors;

public class AdApiResponse {

    private List<Campaigns> campaigns;

    public List<AdEntity> getAdEntity(long updateTime){
        return campaigns.stream().map(campaign -> new AdEntity(campaign, updateTime, Category.AD)).collect(Collectors.toList());
    }
    public class Campaigns{
        private int id;
        private String name;
        private String imageUrl;
        private int firstDisplayPriority;
        private int firstDisplayWeight;
        private int frequency;
        private String landing_url;



        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public int getFirstDisplayPriority() {
            return firstDisplayPriority;
        }

        public int getFirstDisplayWeight() {
            return firstDisplayWeight;
        }

        public int getFrequency() {
            return frequency;
        }

        public String getLanding_url() {
            return landing_url;
        }
    }
}
