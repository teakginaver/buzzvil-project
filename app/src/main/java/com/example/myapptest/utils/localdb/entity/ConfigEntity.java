package com.example.myapptest.utils.localdb.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.myapptest.utils.network.response.ConfigApiResponse;

@Entity(tableName = "config")
public class ConfigEntity {
    @PrimaryKey
    private int id;
    private String firstAdRatio;

    //광고와 컨텐츠를 firstAdRatio으로 보여주기 위해서 사용할 필드 업데이트(첫화면 선정될때 1씩 증가)
    private int firstViewCnt;

    public ConfigEntity(@NonNull ConfigApiResponse response) {
        id = 1;
        this.firstAdRatio = response.getFirstAdRatio();
        firstViewCnt = 0;
    }

    public ConfigEntity(String ratio) {
        id = 1;
        this.firstAdRatio = ratio;
        firstViewCnt = 0;
    }

    public ConfigEntity() {}


    //광고 수
    public int getAdCnt() throws Exception{
        return Integer.valueOf(firstAdRatio.split(":")[0]);
    }

    //컨텐츠 수
    public int getContentsCnt() throws Exception{
        return Integer.valueOf(firstAdRatio.split(":")[1]);
    }

    //광고 + 컨텐츠 수
    public int allCnt() throws Exception{
        return Integer.valueOf(firstAdRatio.split(":")[0]) + Integer.valueOf(firstAdRatio.split(":")[1]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstAdRatio() {
        return firstAdRatio;
    }

    public void setFirstAdRatio(String firstAdRatio) {
        this.firstAdRatio = firstAdRatio;
    }

    public int getFirstViewCnt() {
        return firstViewCnt;
    }

    public void setFirstViewCnt(int firstViewCnt) {
        this.firstViewCnt = firstViewCnt;
    }

    @Override
    public String toString() {
        return "ConfigEntity{" +
                "id=" + id +
                ", firstAdRatio='" + firstAdRatio + '\'' +
                ", firstViewCnt=" + firstViewCnt +
                '}';
    }
}

