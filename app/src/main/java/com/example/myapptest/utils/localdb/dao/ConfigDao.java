package com.example.myapptest.utils.localdb.dao;

import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.example.myapptest.utils.localdb.entity.ConfigEntity;

import org.jetbrains.annotations.NotNull;

@Dao
public interface ConfigDao {
    @Insert
    public void insert(ConfigEntity entity);
    @Update
    public void update(ConfigEntity entity);

    @Query("UPDATE config SET firstViewCnt = firstViewCnt + 1")
    public int updateConfigViewCnt();

    @Query("SELECT * FROM config")
    public ConfigEntity findConfig();

}
