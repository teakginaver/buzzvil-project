package com.example.myapptest.utils;

/**
 *
 *  서버 API 호출시 로딩 보여주는 Interface
 *
 * */
public interface ProgressInterface {
    public void startProgress();
    public void endProgress();
}
