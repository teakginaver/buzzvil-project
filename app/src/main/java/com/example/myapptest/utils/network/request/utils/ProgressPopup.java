package com.example.myapptest.utils.network.request.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import com.example.myapptest.R;

public class ProgressPopup extends Dialog {
    public ProgressPopup(Context context) {
        super(context, R.style.ThemeTransparentBackground);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.popupprogress);
    }
}
