package com.example.myapptest.utils.network.response;

import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;

import java.util.List;
import java.util.stream.Collectors;

public class ContentsApiResponse {

    private List<AdApiResponse.Campaigns> campaigns;

    public List<CampaignsEntity> getCampaignsEntity(long updateTime){
        return campaigns.stream().map(campaign -> new CampaignsEntity(campaign, updateTime, Category.CONTENTS)).collect(Collectors.toList());
    }
}
