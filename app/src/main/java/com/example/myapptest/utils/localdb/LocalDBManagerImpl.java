package com.example.myapptest.utils.localdb;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Room;

import com.example.myapptest.utils.CampaignsSorter;
import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.utils.localdb.dao.CampaignsDao;
import com.example.myapptest.utils.localdb.dao.ConfigDao;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.CampaignsSubInfo;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.views.viewpager.PageItem;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class LocalDBManagerImpl implements LocalDBManager{
    private final String DATABASE_NAME = "localdb";
    private static LocalDatabase db;

    public LocalDBManagerImpl(@NotNull Context context) {
        if(db == null){
            db = Room.databaseBuilder(context, LocalDatabase.class, DATABASE_NAME).build();
        }
    }

    @Override
    public void close() {
        db.close();
        db = null;
    }

    private CampaignsDao getCampaignsDao() { return db.getCampaignsDao();}
    private ConfigDao getConfigDao() {return db.getConfigDao();}

    @Override
    public List<PageItem> getAllCampaigns() {
        ConfigEntity config = findConfig();
        List<CampaignsEntity> entities = findCampaignsList();
        return new CampaignsSorter(config, entities).start();
    }

    public void insertOrUpdate(@NotNull final List<CampaignsEntity> entitys){
        for (CampaignsEntity entity : entitys) {
            try{
                insert(entity);
            }catch (SQLiteConstraintException e){
                CampaignsSubInfo subInfo = new CampaignsSubInfo(entity);
                updateSubInfo(subInfo);
            }
        }
    }

    public void insertOrUpdate(@NotNull final ConfigEntity entity){
        try{
            insert(entity);
        }catch (SQLiteConstraintException e){
            ConfigEntity oldConfig = findConfig();
            if(!oldConfig.getFirstAdRatio().equals(entity.getFirstAdRatio())){
                update(entity);
            }
        }
    }

    @Override
    public void insert(@NotNull final CampaignsEntity entity) {
        getCampaignsDao().insert(entity);
    }

    @Override
    public void updateSubInfo(@NotNull final CampaignsSubInfo subInfo) {
        getCampaignsDao().updateSubInfo(subInfo);
    }

    @Override
    public void deleteByUpdateTime(final long updateTime, @NotNull final Category category) {
        getCampaignsDao().deleteByUpdateTime(updateTime, category);
    }

    @Override
    public void deleteById(final int id) {
        getCampaignsDao().deleteById(id);
    }

    @Override
    public int updateViewCnt(final int viewcnt, final int id) {
        return getCampaignsDao().updateViewCnt(viewcnt, id);
    }

    @Override
    public int updateFavorites(@NotNull final Favorites favorites, final int id) {
        return getCampaignsDao().updateFavorites(favorites, id);
    }

    @Override
    public List<CampaignsEntity> findCampaignsList() {
        return getCampaignsDao().findCampaignsList();
    }

    @Override
    public void insert(@NotNull final ConfigEntity entity) {
        getConfigDao().insert(entity);
    }

    @Override
    public void update(@NotNull final ConfigEntity entity){
        getConfigDao().update(entity);
    }

    @Override
    public int updateConfigViewCnt() {
        return getConfigDao().updateConfigViewCnt();
    }

    @Override
    public ConfigEntity findConfig() {
        return getConfigDao().findConfig();
    }
}
