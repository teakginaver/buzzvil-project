package com.example.myapptest.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

import com.example.myapptest.R;

/**
 * 
 * API를 통해서 데이터를 받아올때 나타나는 프로그래스 팝업
 * 
 * */
public class ProgressPopup extends Dialog {
    public ProgressPopup(Context context) {
        super(context, R.style.ThemeTransparentBackground);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.setContentView(R.layout.popupprogress);
    }
}
