package com.example.myapptest.utils.localdb;

import com.example.myapptest.utils.localdb.dao.CampaignsDao;
import com.example.myapptest.utils.localdb.dao.ConfigDao;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.List;

public interface LocalDBManager extends CampaignsDao, ConfigDao {
    List<PageItem> getAllCampaigns();

    void insertOrUpdate(List<CampaignsEntity> entitys);
    void insertOrUpdate(ConfigEntity entity);

    void close();
}
