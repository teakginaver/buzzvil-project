package com.example.myapptest.utils.network.response;

import com.example.myapptest.utils.localdb.entity.ConfigEntity;

public class ConfigApiResponse {

    private String firstAdRatio;

    public String getFirstAdRatio() {
        return firstAdRatio;
    }

    public ConfigEntity getConfigEntity(){
        return new ConfigEntity(this);
    }

    public void setFirstAdRatio(String firstAdRatio) {
        this.firstAdRatio = firstAdRatio;
    }
}
