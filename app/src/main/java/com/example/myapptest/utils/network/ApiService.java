package com.example.myapptest.utils.network;

import com.example.myapptest.utils.network.response.AdApiResponse;
import com.example.myapptest.utils.network.response.ConfigApiResponse;
import com.example.myapptest.utils.network.response.ContentsApiResponse;
import io.reactivex.Observable;
import retrofit2.http.*;


/**
 *
 *  서버 API 호출시 URL 및 Method - retrofit2
 *
 * */

public interface ApiService {

    @GET("/buzzvi.test/test_config.json")
    Observable<ConfigApiResponse> getConfig();

    @GET("/buzzvi.test/test_articles.json")
    Observable<ContentsApiResponse> getContentsList();

    @GET("/buzzvi.test/test_ads.json")
    Observable<AdApiResponse> getAdList();

}
