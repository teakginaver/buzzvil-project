package com.example.myapptest.utils.network.request.utils.localdb.dao;

import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapptest.utils.network.request.utils.localdb.entity.AdEntity;
import com.example.myapptest.utils.network.request.utils.localdb.entity.AdSubInfo;

import java.util.List;

@Dao
public abstract class AdDao {
    @Insert
    public abstract void insert(AdEntity entity);
    @Update
    public abstract void update(AdEntity entity);

    @Update(entity = AdEntity.class)
    public abstract void updateSubInfo(AdSubInfo subInfo);

    @Query("DELETE FROM ad WHERE local_datetime <> :updateTime")
    public abstract void deleteByUpdateTime(long updateTime);

    @Query("UPDATE ad SET view_cnt = :viewcnt WHERE id = :id")
    public abstract void updateByViewCnt(int viewcnt, int id);

    @Query("select * from ad where frequency > view_cnt order by firstDisplayPriority")
    public abstract List<AdEntity> getAll();

    public void insertOrUpdate(List<AdEntity> entitys){
        for (AdEntity entity : entitys) {
            try{
                insert(entity);
            }catch (SQLiteConstraintException e){
                AdSubInfo subInfo = new AdSubInfo(entity);
                updateSubInfo(subInfo);
            }
        }
    }
}
