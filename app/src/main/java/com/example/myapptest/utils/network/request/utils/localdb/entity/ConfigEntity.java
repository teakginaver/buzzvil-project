package com.example.myapptest.utils.network.request.utils.localdb.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.myapptest.utils.network.request.utils.network.response.ConfigApiResponse;

@Entity(tableName = "config")
public class ConfigEntity {
    @PrimaryKey
    private int id;
    private String firstAdRatio;
    private int firstViewCnt;

    public ConfigEntity(ConfigApiResponse response) {
        id = 1;
        this.firstAdRatio = response.getFirstAdRatio();
        firstViewCnt = 0;
    }

    public ConfigEntity() {}

    public String getFirstAdRatio() {
        return firstAdRatio;
    }

    public int getAdCnt() throws Exception{
        return Integer.valueOf(firstAdRatio.split(":")[0]);
    }

    public int getContentsCnt() throws Exception{
        return Integer.valueOf(firstAdRatio.split(":")[1]);
    }

    public int allCnt() throws Exception{
        return Integer.valueOf(firstAdRatio.split(":")[0]) + Integer.valueOf(firstAdRatio.split(":")[1]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFirstViewCnt() {
        return firstViewCnt;
    }

    public void setFirstViewCnt(int firstViewCnt) {
        this.firstViewCnt = firstViewCnt;
    }

    public void setFirstAdRatio(String firstAdRatio) {
        this.firstAdRatio = firstAdRatio;
    }
}
