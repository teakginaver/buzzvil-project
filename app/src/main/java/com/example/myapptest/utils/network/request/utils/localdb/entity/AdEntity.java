package com.example.myapptest.utils.network.request.utils.localdb.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.myapptest.utils.network.request.utils.network.response.AdApiResponse;

@Entity(tableName = "ad")
public class AdEntity {
    @PrimaryKey
    private int id;
    private String name;
    private String imageUrl;
    private int firstDisplayPriority;
    private int firstDisplayWeight;
    private int frequency;
    private String landing_url;


    public Category category;// AD, CONTENTS 구분
    private int view_cnt;// 지금까지 보여준 뷰 카운터
    private long local_datetime;
    public AdEntity() {}

    public AdEntity(AdApiResponse.Campaigns response, long local_datetime, Category category) {
        this.id = response.getId();
        this.name = response.getName();
        this.imageUrl = response.getImageUrl();
        this.firstDisplayPriority = response.getFirstDisplayPriority();
        this.firstDisplayWeight = response.getFirstDisplayWeight();
        this.frequency = response.getFrequency();
        this.landing_url = response.getLanding_url();
        this.view_cnt = 0;
        this.local_datetime = local_datetime;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getFirstDisplayPriority() {
        return firstDisplayPriority;
    }

    public int getFirstDisplayWeight() {
        return firstDisplayWeight;
    }

    public int getFrequency() {
        return frequency;
    }

    public String getLanding_url() {
        return landing_url;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setFirstDisplayPriority(int firstDisplayPriority) {
        this.firstDisplayPriority = firstDisplayPriority;
    }

    public void setFirstDisplayWeight(int firstDisplayWeight) {
        this.firstDisplayWeight = firstDisplayWeight;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public void setLanding_url(String landing_url) {
        this.landing_url = landing_url;
    }

    public int getView_cnt() {
        return view_cnt;
    }

    public void setView_cnt(int view_cnt) {
        this.view_cnt = view_cnt;
    }

    public long getLocal_datetime() {
        return local_datetime;
    }

    public void setLocal_datetime(long local_datetime) {
        this.local_datetime = local_datetime;
    }

    @Override
    public String toString() {
        return "AdEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", firstDisplayPriority=" + firstDisplayPriority +
                ", firstDisplayWeight=" + firstDisplayWeight +
                ", frequency=" + frequency +
                ", landing_url='" + landing_url + '\'' +
                ", view_cnt=" + view_cnt +
                ", local_datetime=" + local_datetime +
                '}';
    }
}
