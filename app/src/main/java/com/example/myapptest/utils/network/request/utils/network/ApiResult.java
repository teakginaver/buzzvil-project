package com.example.myapptest.utils.network.request.utils.network;

public interface ApiResult {
    public void onResult(Object o);
    public void onError(Throwable throwable);
}
