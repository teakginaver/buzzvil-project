package com.example.myapptest.utils.network;

/**
 *
 *  서버 API 호출시 결과 받는 Interface
 *
 * */
public interface ApiResult {
    public void onResult(Object o);
    public void onError(Throwable throwable);
}
