package com.example.myapptest.utils.network.request.utils.network;

import android.util.Log;

import com.example.myapptest.utils.network.request.utils.ProgressInterface;
import com.example.myapptest.utils.network.request.utils.network.request.BaseRequest;

import java.lang.reflect.Method;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import io.reactivex.Observable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.HttpException;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory {
    private final String DOMAIN = "https://s3-ap-northeast-1.amazonaws.com";
    private static final String TAG = "APIFactory";

    private ApiResult apiResult;
    private List<BaseRequest> requestList;

    private int requestCnt = 0;
    public ApiFactory(ApiResult apiResult, List<BaseRequest> requestList) {
        this.apiResult = apiResult;
        this.requestList = requestList;

    }

    private final TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {}

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {}

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }
            }
    };



    private OkHttpClient defaultClient() {
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        SSLContext sslContext = null;
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);

            sslContext = SSLContext.getInstance("TLS");

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, "keystore_pass".toCharArray());
            sslContext.init(null, trustAllCerts, new SecureRandom());
        } catch (Exception e) {
            e.printStackTrace();
        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS) // 연결 타임아웃
                .readTimeout(30, TimeUnit.SECONDS) // 읽기 타임아웃
                .writeTimeout(30, TimeUnit.SECONDS) // 쓰기 타임아웃
                .addInterceptor(chain -> {
                    Request.Builder builder = chain.request().newBuilder();
                    builder.header("User-Agent", "Mozilla/5.0 (Linux; Android 8.0.0; SM-G955U Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Mobile Safari/537.36");
                    return chain.proceed(builder.build());
                })
                //.addInterceptor(interceptor)
                .build();
        return okHttpClient;
    }

    public void newThread(){
        ApiService o = createApiService();

        for (BaseRequest request : requestList) {
            Observable observable = findMethod(o, request);
            if(observable == null){
                onError();
                return;
            }
            observable.subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.single())
                    .subscribe(onNext, onError, onComplete);
        }
    }

    private Observable findMethod(Object service, BaseRequest request){
        String method = request.getMethod();

        Method m = null;
        try {
            m = service.getClass().getMethod(method);
            //Log.e(TAG,  "method : " + m.getName());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            onError();
            new NoSuchMethodException();
        }

        Observable observable = null;
        try {
            observable = (Observable)m.invoke(service);
        } catch (Exception e) {
            e.printStackTrace();
            onError();
            new IllegalAccessException();
        }
        return observable;
    }

    private ApiService createApiService() {
        activitityLoadingStart();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(DOMAIN)
                .client(defaultClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(ApiService.class);
    }


    public static String getErrorBody(Throwable throwable) {
        String result = null;
        if (throwable != null && throwable instanceof HttpException) {
            Response response = ((HttpException) throwable).response();

            try {
                result = response.message();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }



    public void activitityLoadingStart(){
        if(apiResult != null && (apiResult instanceof ProgressInterface)){
            if(requestCnt == 0){
                requestCnt = requestList.size();
                ((ProgressInterface)apiResult).startProgress();
            }
        }
    }

    public void activitityLoadingEnd(){
        if(apiResult != null && apiResult instanceof ProgressInterface){
            requestCnt--;
            if(requestCnt <= 0){
                ((ProgressInterface)apiResult).endProgress();
            }
        }
    }


    private Consumer onNext = item ->{
        apiResult.onResult(item);
    };

    private Consumer onError = t -> {
        activitityLoadingEnd();
        Throwable throwable = (Throwable)t;
        throwable.printStackTrace();
        apiResult.onError(throwable);
    };

    private void onError(){
        activitityLoadingEnd();
        apiResult.onError(new Throwable());
    }

    private Action onComplete = () ->{
        activitityLoadingEnd();
    };


    public static List<BaseRequest> makeRequest(BaseRequest... requests){
        List<BaseRequest> a = Arrays.asList(requests);
        return Arrays.asList(requests);
    }
}
