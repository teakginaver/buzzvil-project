package com.example.myapptest.views.hiddenviews;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.example.myapptest.R;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.views.MainViewInterface;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public final class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.FavoritesHolder> {
    final private MainViewInterface mainViewInterface;
    private List<PageItem> list;
    public FavoritesAdapter(MainViewInterface mainViewInterface, List<PageItem> list) {
        this.mainViewInterface = mainViewInterface;
        this.list = list;
    }

    public void setList(@NonNull List<PageItem> list){
        this.list = list;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FavoritesHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_favorites, parent, false);
        
        //뷰 클릭시 해당 캠페인으로 이동
        view.findViewById(R.id.allLayout).setOnClickListener(v -> {
            PageItem item = getPageItem(v);
            if(item != null)mainViewInterface.movePage(item);
        });
        
        //즐겨찾기 추가, 제거 버튼 이벤트
        view.findViewById(R.id.favoritesIcon).setOnClickListener(v -> {
            PageItem item = getPageItem(v);
            if(item != null)mainViewInterface.setFavorites((ImageView)v, item, false);
        });

        return new FavoritesHolder(view);
    }

    private PageItem getPageItem(View v){
        if(v.getTag() == null || !(v.getTag() instanceof PageItem))return null;
        return (PageItem)v.getTag();
    }

    @Override
    public void onBindViewHolder(@NonNull FavoritesHolder holder, int position) {
        holder.bindView(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class FavoritesHolder extends RecyclerView.ViewHolder {
        private LinearLayout allLayout;
        private ImageView favoritesIcon;
        private TextView nameText;
        private CircleImageView circleImageView;
        public FavoritesHolder(@NonNull View itemView) {
            super(itemView);
            allLayout = itemView.findViewById(R.id.allLayout);
            favoritesIcon = itemView.findViewById(R.id.favoritesIcon);
            nameText = itemView.findViewById(R.id.nameText);
            circleImageView = itemView.findViewById(R.id.circleImageView);
        }

        public void bindView(@NonNull PageItem pageItem){
            nameText.setText(pageItem.getName());

            allLayout.setTag(pageItem);
            favoritesIcon.setTag(pageItem);

            favoritesIcon.setImageResource(R.mipmap.active_favorites);
            if(pageItem.getFavorites() == Favorites.N)
                favoritesIcon.setImageResource(R.mipmap.inactive_favorites);

            RequestBuilder<Drawable> requestBuilder= Glide.with(itemView.getContext())
                    .asDrawable().sizeMultiplier(0.1f);
            Glide.with(itemView.getContext()).load(pageItem.getImageUrl()).thumbnail(requestBuilder).placeholder(R.drawable.loading).into(circleImageView);
        }
    }
}

