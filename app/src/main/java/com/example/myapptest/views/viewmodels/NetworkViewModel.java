package com.example.myapptest.views.viewmodels;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.utils.localdb.LocalDBManager;
import com.example.myapptest.utils.localdb.LocalDBManagerImpl;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.utils.network.ApiFactory;
import com.example.myapptest.utils.network.ApiResult;
import com.example.myapptest.utils.network.request.ConfigApiRequest;
import com.example.myapptest.utils.network.response.AdApiResponse;
import com.example.myapptest.utils.network.response.ConfigApiResponse;
import com.example.myapptest.utils.network.response.ContentsApiResponse;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class NetworkViewModel extends ViewModel implements ApiResult {
    private LocalDBManager localDB;
    private boolean loadConfig = false;
    private boolean loadContents = false;
    private boolean loadAd = false;


    public MutableLiveData<Boolean> isNewNetworkData = new MutableLiveData<Boolean>();

    public void init(LocalDBManager localDB){
        this.localDB = localDB;
        isNewNetworkData.postValue(false);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
    }

    /**
     * 서버에서 API호출 -> 결과는 LocalDB에 입력
     * **/
    public void connectApiData(final Object o) {
        new ApiFactory((ApiResult)o, ApiFactory.makeRequest(new ConfigApiRequest("getConfig")
                , new ConfigApiRequest("getContentsList")
                , new ConfigApiRequest("getAdList"))).newThread();
    }

    @Override
    public void onResult(final Object o) {
        if(o instanceof ConfigApiResponse){saveConfig((ConfigApiResponse)o);}
        if(o instanceof ContentsApiResponse){saveContents((ContentsApiResponse)o);}
        if(o instanceof AdApiResponse){saveAd((AdApiResponse)o);}
    }

    @Override
    public void onError(Throwable throwable) {}

    /**
     * 서버에서 API호출 -> Config 로컬에 저장
     * */
    private void saveConfig(final ConfigApiResponse response){
        ConfigEntity entity = response.getConfigEntity();
        localDB.insertOrUpdate(entity);
        loadConfig = true;
        loadComplete();
    }

    /**
     * 서버에서 API호출 -> Contents 로컬에 저장
     * */
    private void saveContents(final ContentsApiResponse response){
        long updateTime = System.currentTimeMillis();
        List<CampaignsEntity> list = response.getCampaignsEntity(updateTime);
        localDB.insertOrUpdate(list);
        localDB.deleteByUpdateTime(updateTime, Category.CONTENTS);
        loadContents = true;
        loadComplete();
    }

    /**
     * 서버에서 API호출 -> Ad 로컬에 저장
     * */
    private void saveAd(final AdApiResponse response){
        long updateTime = System.currentTimeMillis();
        List<CampaignsEntity> list = response.getCampaignsEntity(updateTime);
        localDB.insertOrUpdate(list);
        localDB.deleteByUpdateTime(updateTime, Category.AD);
        loadAd = true;
        loadComplete();
    }

    private void loadComplete(){
        if(loadConfig && loadContents && loadAd){
            setNewNetworkDataFlag(true);
        }
    }

    public void setNewNetworkDataFlag(boolean b){
        isNewNetworkData.postValue(b);
    }

}
