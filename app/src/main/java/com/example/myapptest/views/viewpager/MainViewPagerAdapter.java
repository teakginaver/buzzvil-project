package com.example.myapptest.views.viewpager;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.example.myapptest.R;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.views.MainViewInterface;

import java.util.List;

public final class MainViewPagerAdapter extends RecyclerView.Adapter<MainViewPagerAdapter.MainViewPagerHolder> {
    final private MainViewInterface mainViewInterface;
    private List<PageItem> list;
    public MainViewPagerAdapter(@NonNull MainViewInterface mainViewInterface, List<PageItem> list) {
        this.mainViewInterface = mainViewInterface;
        this.list = list;
    }

    public void setList(@NonNull List<PageItem> list){
        this.list = list;
        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MainViewPagerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_viewpager, parent, false);
        view.findViewById(R.id.favoritesImageView).setOnClickListener(v -> {
            PageItem item = getPageItem(v);
            if(item != null)mainViewInterface.setFavorites((ImageView) v, item, true);
        });
        return new MainViewPagerHolder(view);
    }

    private PageItem getPageItem(View v){
        if(v.getTag() == null || !(v.getTag() instanceof PageItem))return null;
        return (PageItem)v.getTag();
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewPagerHolder holder, int position) {
        holder.bindView(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MainViewPagerHolder extends RecyclerView.ViewHolder {
        private ImageView contentsImageView;
        private ImageView favoritesImage;
        private TextView nameText;
        public MainViewPagerHolder(@NonNull View itemView) {
            super(itemView);
            contentsImageView = itemView.findViewById(R.id.contentsImageView);
            favoritesImage = itemView.findViewById(R.id.favoritesImageView);
            nameText = itemView.findViewById(R.id.nameText);
        }

        public void bindView(@NonNull PageItem pageItem){
            nameText.setText(pageItem.getName());

            favoritesImage.setTag(pageItem);
            favoritesImage.setImageResource(R.mipmap.active_favorites);
            if(pageItem.getFavorites() == Favorites.N)
                favoritesImage.setImageResource(R.mipmap.inactive_favorites);

            Glide.with(itemView.getContext()).load(pageItem.getImageUrl())
                    .placeholder(R.drawable.loading).into(contentsImageView);
        }
    }
}

