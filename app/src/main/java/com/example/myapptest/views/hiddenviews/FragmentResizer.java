package com.example.myapptest.views.hiddenviews;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

public final class FragmentResizer {
    private int WIDTH;
    private int HEIGHT;

    public FragmentResizer(Context context) {
        int statusBarHeight = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resourceId);
        }
        HEIGHT = context.getResources().getDisplayMetrics().heightPixels - statusBarHeight;
        this.WIDTH =  context.getResources().getDisplayMetrics().widthPixels;
    }

    public int getHEIGHT() {
        return HEIGHT;
    }

    public int getTabHeight(){
        return HEIGHT / 18;
    }


    /**
     * 전체 크기 조정
     * */
    public void makeHiddenLayout(RelativeLayout parent){
        ConstraintLayout.LayoutParams frameLayoutLayoutParams = (ConstraintLayout.LayoutParams) parent.getLayoutParams();
        frameLayoutLayoutParams.height = getHEIGHT() * 3;
        frameLayoutLayoutParams.topMargin = getHEIGHT() * (-1);
    }


    /**
     * 즐겨찾기 크기 조정
     * */
    public void hiddenFavoritesLayout(FrameLayout parent, TextView tabView, View view){
        RelativeLayout.LayoutParams favoritesLayoutParams = (RelativeLayout.LayoutParams) parent.getLayoutParams();

        favoritesLayoutParams.height = HEIGHT;
        favoritesLayoutParams.topMargin = getTabHeight();

        FrameLayout.LayoutParams tabViewLayoutParams = (FrameLayout.LayoutParams) tabView.getLayoutParams();
        tabViewLayoutParams.height = getTabHeight();

        FrameLayout.LayoutParams viewLayoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
        viewLayoutParams.height = HEIGHT - getTabHeight() - 1;
    }


    /**
     * 상세보기 크기 조정
     * */
    public void hiddenContentsLayout(FrameLayout parent, TextView tabView, View view, TextView stopView){
        RelativeLayout.LayoutParams favoritesLayoutParams = (RelativeLayout.LayoutParams) parent.getLayoutParams();

        favoritesLayoutParams.height = HEIGHT;
        favoritesLayoutParams.topMargin = (HEIGHT * 2) - getTabHeight();

        LinearLayout.LayoutParams tabViewLayoutParams = (LinearLayout.LayoutParams) tabView.getLayoutParams();
        tabViewLayoutParams.height = getTabHeight();

        LinearLayout.LayoutParams viewLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
        viewLayoutParams.height = HEIGHT - getTabHeight() - getTabHeight();

        LinearLayout.LayoutParams stopLayoutParams = (LinearLayout.LayoutParams) stopView.getLayoutParams();
        stopLayoutParams.height = getTabHeight();
    }

}
