package com.example.myapptest.views;

import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.example.myapptest.views.hiddenviews.FragmentResizer;
import com.example.myapptest.views.viewmodels.LocalDBViewModel;
import com.example.myapptest.views.viewmodels.NetworkViewModel;
import com.example.myapptest.views.viewpager.PageItem;

/**
 * 
 * MainActivity 인터페이스
 * 
 * */
public interface MainViewInterface {
    public FragmentResizer getFragmentResizer();//Fragment 화면 크기 조정
    public LocalDBViewModel getDBViewModel();
    public FrameLayout getFavoritesLayout();//즐겨찾기 Layout
    public FrameLayout getContentsLayout();// 상세보기 Layout
    public void stopContents();//캠페인 그만보기 이벤트
    public void setFavorites(@NonNull ImageView v, PageItem item, boolean fromViewPager);// 즐겨찾기 추가 이벤트
    public void findFavoritesList();//즐겨찾기 목록
    public void moveFirstPage();//viewPager 페이지 이동 - 첫페이지로
    public void movePage(PageItem item);//viewPager 페이지 이동 - 원하는 페이지로
}
