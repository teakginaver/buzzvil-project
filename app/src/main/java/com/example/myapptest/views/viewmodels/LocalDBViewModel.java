package com.example.myapptest.views.viewmodels;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.utils.localdb.LocalDBManager;
import com.example.myapptest.utils.localdb.LocalDBManagerImpl;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.utils.network.ApiFactory;
import com.example.myapptest.utils.network.ApiResult;
import com.example.myapptest.utils.network.request.ConfigApiRequest;
import com.example.myapptest.utils.network.response.AdApiResponse;
import com.example.myapptest.utils.network.response.ConfigApiResponse;
import com.example.myapptest.utils.network.response.ContentsApiResponse;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class LocalDBViewModel extends ViewModel {
    private LocalDBManager localDB;

    private long LOADTIMER = 1000l;
    private long loadTimer = 0l;
    private boolean firstPage = false;

    public MutableLiveData<List<PageItem>> pageItem = new MutableLiveData<List<PageItem>>();
    public MutableLiveData<List<PageItem>> favoritesItem = new MutableLiveData<List<PageItem>>();
    public MutableLiveData<String> webUrl = new MutableLiveData<String>();

    public LocalDBViewModel() {
    }

    public void init(LocalDBManager localDB){
        this.localDB = localDB;
        pageItem.postValue(new ArrayList<>());
        favoritesItem.postValue(new ArrayList<>());
        webUrl.postValue("https://www.buzzvil.com/");// 초기 URL
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if(localDB != null)localDB.close();
    }

    /**
     * LocalDB에서 캠페인 리스트 받기
     * **/
    public void getDBData() {
        Observable.fromCallable(() -> localDB.getAllCampaigns())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(item ->{
                    pageItem.postValue((List<PageItem>) item);
                    long timer = System.currentTimeMillis();
                    if(timer > this.loadTimer + LOADTIMER){
                        this.loadTimer = timer;
                        localDB.updateConfigViewCnt();
                    }
                });
    }


    /**
     * PageView 페이지 이동시 WebView url 얻기 -> LiveData로 바로 보냄
     * */
    public void getWebUrl(final int index){
        try{
            if(pageItem.getValue() == null)return;
            webUrl.postValue(pageItem.getValue().get(index).getLanding_url());
        }catch (Exception e){e.printStackTrace();}
    }

    /**
     * 캠페인 그만보기 클릭 이벤트
     * */
    public void deleteCampaign(final int index){
        try{
            PageItem item = pageItem.getValue().get(index);
            pageItem.getValue().remove(item);

            Observable<Integer> source = Observable.fromCallable(() -> localDB.updateViewCnt(item.getFrequency() + 1, item.getId()));
            source.subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .subscribe(result -> pageItem.postValue(pageItem.getValue()));
        }catch (Exception e){e.printStackTrace();}
    }

    /**
     * 즐겨찾기 설정
     * */
    public void setFavorites(final PageItem item){
        try{
            if(item.getFavorites() == Favorites.N)item.setFavorites(Favorites.Y);
            else item.setFavorites(Favorites.N);

            Observable.fromCallable(() -> localDB.updateFavorites(item.getFavorites(), item.getId()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .subscribe(result -> {});
        }catch (Exception e){e.printStackTrace();}
    }


    /**
     * 캠페인 viewCnt++
     * */
    public void viewCntpp(final int index){
        try{
            PageItem item = pageItem.getValue().get(index);
            item.setView_cnt(item.getView_cnt() + 1);

            Observable.fromCallable(() -> localDB.updateViewCnt(item.getView_cnt(), item.getId()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.computation())
                    .subscribe(onNext -> {});
        }catch (Exception e){e.printStackTrace();}
    }

    /**
     * 즐겨찾기 List - ViewPager List에서 가져오기
     * */
    public void findFavoritesList() {
        List<PageItem> list = pageItem.getValue().stream().filter(i -> i.getFavorites() == Favorites.Y).collect(Collectors.toList());
        favoritesItem.postValue(list);
    }



    public int findPositionByPageItem(PageItem item){
        int index = 0;
        for (PageItem i : pageItem.getValue()) {
            if(i == item)return index;
            index++;
        }
        return -1;
    }

    //첫페이지로 이동 플래그
    public boolean isFirstPage() {return firstPage;}
    public void setFirstPage(boolean firstPage) {this.firstPage = firstPage;}
}
