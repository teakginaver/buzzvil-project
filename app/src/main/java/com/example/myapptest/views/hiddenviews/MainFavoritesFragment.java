package com.example.myapptest.views.hiddenviews;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.myapptest.databinding.FragmentFavoritesBinding;
import com.example.myapptest.views.MainViewInterface;

import java.util.ArrayList;


public class MainFavoritesFragment extends Fragment implements View.OnTouchListener {
    private MainViewInterface mainViewInterface;
    private FragmentResizer fragmentResizer;

    private FragmentFavoritesBinding binding;

    private float startPositionY;
    private TabDragType tabDragType = TabDragType.UP;

    public MainFavoritesFragment(MainViewInterface mainViewInterface) {
        this.mainViewInterface = mainViewInterface;
        fragmentResizer = mainViewInterface.getFragmentResizer();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.toptab.setOnTouchListener(this);
        binding.recyclerView.setAdapter(new FavoritesAdapter(mainViewInterface, new ArrayList<>()));

        mainViewInterface.getDBViewModel().favoritesItem.observe((AppCompatActivity)getContext(), pageItems -> {
            ((FavoritesAdapter)binding.recyclerView.getAdapter()).setList(pageItems);
            visibleEmptyMessage(pageItems.size() > 0);
        });
    }

    //즐겨찾기 없습니다. 문구 표시
    private void visibleEmptyMessage(boolean visible){
        binding.emptyMessage.setVisibility(View.VISIBLE);
        if(visible)binding.emptyMessage.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        reSizeAndHidden();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void reSizeAndHidden(){
        fragmentResizer.hiddenFavoritesLayout(mainViewInterface.getFavoritesLayout(), binding.toptab, binding.recyclerView);
    }

    public void hidden(){
        final int HEIGHT = fragmentResizer.getHEIGHT();
        animationDrag((int)((float)HEIGHT * 0.2f));
    }

    //////////////////////////// Drag 움직임

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(view.equals(binding.toptab)){
            int action = motionEvent.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    startDrag(motionEvent.getRawY());break;
                case MotionEvent.ACTION_MOVE:
                    moveDrag(motionEvent.getRawY());break;
                case MotionEvent.ACTION_UP:
                    animationDrag((int)motionEvent.getRawY());break;
                default: break;
            }
            return true;
        }
        return false;
    }

    private void startDrag(float y){
        final int HEIGHT = fragmentResizer.getHEIGHT();

        startPositionY = y;
        tabDragType = TabDragType.UP;
        if(startPositionY < (HEIGHT / 2))tabDragType = TabDragType.DAWN;

        setVisibleContentsLayout(View.INVISIBLE);

        //드래그 시작할때 리스트 갱신
        if(tabDragType == TabDragType.DAWN)mainViewInterface.findFavoritesList();
    }

    private void moveDrag(final float y){
        final int HEIGHT = fragmentResizer.getHEIGHT();
        final int TABHEIGHT = fragmentResizer.getTabHeight();

        float margine = HEIGHT - (startPositionY - y);//TabDrag.UP
        if(tabDragType == TabDragType.DAWN)margine = y - startPositionY;

        if(margine > HEIGHT || margine <= TABHEIGHT)return;//드래그가 화면을 벗어나지 안도록
        mainViewInterface.getFavoritesLayout().setY(margine);
    }

    private void animationDrag(int y){//endDrag -> animation 효과 추가
        final FrameLayout parent = mainViewInterface.getFavoritesLayout();
        final int HEIGHT = fragmentResizer.getHEIGHT();
        final int TABHEIGHT = fragmentResizer.getTabHeight();
        float openRate =  (float)HEIGHT * 0.2f;
        if(tabDragType == TabDragType.UP)openRate =  (float)HEIGHT * 0.8f;

        if(y > openRate){
            setVisibleContentsLayout(View.INVISIBLE);
            try{ parent.animate().setDuration((long)((HEIGHT - y) / 4)).y((float)HEIGHT);
            }catch (Exception e){ parent.animate().setDuration(100).y((float)HEIGHT); }
        }else{
            setVisibleContentsLayout(View.VISIBLE);
            try{ parent.animate().setDuration((long)(y / 4)).y((float)TABHEIGHT);
            }catch (Exception e){ parent.animate().setDuration(100).y((float)TABHEIGHT); }
        }
    }

    private void setVisibleContentsLayout(int visible){
        mainViewInterface.getContentsLayout().setVisibility(visible);
    }
}
