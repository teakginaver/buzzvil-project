package com.example.myapptest.views.viewpager;

import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.Favorites;

import java.lang.reflect.Field;

public class PageItem {
    private int id;
    private String name;
    private String imageUrl;
    private int firstDisplayPriority;
    private int firstDisplayWeight;
    private int frequency;
    private String landing_url;
    private int view_cnt;// 지금까지 보여준 뷰 카운터
    private Favorites favorites;
    private long local_datetime;

    private Category category;

    public PageItem(Object o) {
        Field[] f = o.getClass().getDeclaredFields();
        for (Field objectField : f) {
            try {
                String name = objectField.getName();
                Field thisField = this.getClass().getDeclaredField(name);
                thisField.setAccessible(true);
                objectField.setAccessible(true);
                thisField.set(this, objectField.get(o));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getFirstDisplayPriority() {
        return firstDisplayPriority;
    }

    public void setFirstDisplayPriority(int firstDisplayPriority) {
        this.firstDisplayPriority = firstDisplayPriority;
    }

    public int getFirstDisplayWeight() {
        return firstDisplayWeight;
    }

    public void setFirstDisplayWeight(int firstDisplayWeight) {
        this.firstDisplayWeight = firstDisplayWeight;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getLanding_url() {
        return landing_url;
    }

    public void setLanding_url(String landing_url) {
        this.landing_url = landing_url;
    }

    public int getView_cnt() {
        return view_cnt;
    }

    public void setView_cnt(int view_cnt) {
        this.view_cnt = view_cnt;
    }

    public long getLocal_datetime() {
        return local_datetime;
    }

    public void setLocal_datetime(long local_datetime) {
        this.local_datetime = local_datetime;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Favorites getFavorites() {
        return favorites;
    }

    public void setFavorites(Favorites favorites) {
        this.favorites = favorites;
    }

    @Override
    public String toString() {
        return "PageItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", firstDisplayPriority=" + firstDisplayPriority +
                ", firstDisplayWeight=" + firstDisplayWeight +
                ", frequency=" + frequency +
                ", landing_url='" + landing_url + '\'' +
                ", view_cnt=" + view_cnt +
                ", favorites=" + favorites +
                ", local_datetime=" + local_datetime +
                ", category=" + category +
                '}';
    }
}
