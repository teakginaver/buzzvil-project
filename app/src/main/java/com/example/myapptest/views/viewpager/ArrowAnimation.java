package com.example.myapptest.views.viewpager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.example.myapptest.R;

public final class ArrowAnimation {
    private final Context context;
    private final int FRAME_TIME = 450;
    public ArrowAnimation(Context context) {
        this.context = context;
    }

    public void start(ImageView leftView, ImageView rightView){
        AnimationDrawable leftAni = getLeftAnimation();
        AnimationDrawable rightAni = getRightAnimation();
        leftView.setImageDrawable(leftAni);
        rightView.setImageDrawable(rightAni);
        leftAni.start();
        rightAni.start();
    }

    private AnimationDrawable getLeftAnimation() {
        AnimationDrawable ani = new AnimationDrawable();
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_left_one), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_left_two), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_left_one), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_left_two), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_left_one), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_left_two), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_empty), FRAME_TIME);
        ani.setOneShot(true);
        return ani;
    }

    private AnimationDrawable getRightAnimation() {
        AnimationDrawable ani = new AnimationDrawable();
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_right_one), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_right_two), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_right_one), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_right_two), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_right_one), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_right_two), FRAME_TIME);
        ani.addFrame(ContextCompat.getDrawable(context, R.drawable.arrow_empty), FRAME_TIME);
        ani.setOneShot(true);
        return ani;
    }
}
