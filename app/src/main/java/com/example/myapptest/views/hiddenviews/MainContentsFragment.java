package com.example.myapptest.views.hiddenviews;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.myapptest.databinding.FragmentContentsBinding;
import com.example.myapptest.utils.WebViewUtils;
import com.example.myapptest.views.MainViewInterface;


public class MainContentsFragment extends Fragment implements View.OnTouchListener {
    private MainViewInterface mainViewInterface;
    private FragmentResizer fragmentResizer;
    private FragmentContentsBinding binding;

    private float startPositionY;
    private TabDragType tabDragType = TabDragType.UP;

    public MainContentsFragment(MainViewInterface mainViewInterface) {
        this.mainViewInterface = mainViewInterface;
        this.fragmentResizer = mainViewInterface.getFragmentResizer();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentContentsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebViewUtils.settings(binding.webview);
        binding.bottomtab.setOnTouchListener(this);

        binding.contentsStopText.setOnClickListener(v -> mainViewInterface.stopContents());

        mainViewInterface.getDBViewModel().webUrl.observe((AppCompatActivity)getContext(),
                url -> binding.webview.loadUrl(url));
    }

    @Override
    public void onStart() {
        super.onStart();
        reSizeAndHidden();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void reSizeAndHidden(){
        fragmentResizer.hiddenContentsLayout(mainViewInterface.getContentsLayout(), binding.bottomtab, binding.webview, binding.contentsStopText);
    }

    public void hidden(){
        final int HEIGHT = fragmentResizer.getHEIGHT();
        animationDrag( (int)((float)HEIGHT * 0.83f));
    }


    //////////////////////////// Drag 움직임

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if(view.equals(binding.bottomtab)){
            int action = motionEvent.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    startDrag(motionEvent.getRawY());break;
                case MotionEvent.ACTION_MOVE:
                    moveDrag(motionEvent.getRawY());break;
                case MotionEvent.ACTION_UP:
                    animationDrag((int)motionEvent.getRawY());break;
                default: break;
            }
            return true;
        }
        return false;
    }

    private void startDrag(float y){
        final int HEIGHT = fragmentResizer.getHEIGHT();

        startPositionY = y;
        tabDragType = TabDragType.UP;
        if(startPositionY < (HEIGHT / 2))tabDragType = TabDragType.DAWN;
        setVisibleFavoritesLayout(View.INVISIBLE);
    }

    private void moveDrag(final float y){
        final int HEIGHT = fragmentResizer.getHEIGHT();
        final int TABHEIGHT = fragmentResizer.getTabHeight();
        final int MAXTOP = (HEIGHT * 2) - TABHEIGHT;

        int margine = (int)(HEIGHT + (y - startPositionY));//TabDrag.DOWN
        if(tabDragType == TabDragType.UP)margine = (int)(MAXTOP - (startPositionY - y));

        if(margine > MAXTOP || margine <= HEIGHT)return;//드래그가 화면을 벗어나지 안도록
        mainViewInterface.getContentsLayout().setY(margine);
    }

    private void animationDrag(int y){//endDrag -> animation 효과 추가
        final FrameLayout parent = mainViewInterface.getContentsLayout();
        final int HEIGHT = fragmentResizer.getHEIGHT();
        final int TABHEIGHT = fragmentResizer.getTabHeight();
        float openRate =  (float)HEIGHT * 0.2f;
        if(tabDragType == TabDragType.UP)openRate =  (float)HEIGHT * 0.8f;

        if(y > openRate){
            setVisibleFavoritesLayout(View.VISIBLE);
            try{ parent.animate().setDuration((HEIGHT - y) / 4).y((float)(HEIGHT * 2) - TABHEIGHT);
            }catch (Exception e){ parent.animate().setDuration(100).y((float)(HEIGHT * 2) - TABHEIGHT); }
        }else{
            setVisibleFavoritesLayout(View.INVISIBLE);
            try{ parent.animate().setDuration(y / 4).y((float)HEIGHT);
            }catch (Exception e){ parent.animate().setDuration(100).y((float)HEIGHT); }
        }
    }

    private void setVisibleFavoritesLayout(int visible){
        mainViewInterface.getFavoritesLayout().setVisibility(visible);
    }
}
