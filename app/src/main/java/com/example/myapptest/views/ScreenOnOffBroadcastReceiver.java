package com.example.myapptest.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 *
 * 화면 on/off 시 BroadcastReceiver - Off 시에만 새로 그려주도록.
 *
 * */
public class ScreenOnOffBroadcastReceiver extends BroadcastReceiver {
    private final MainViewInterface mainViewInterface;

    public ScreenOnOffBroadcastReceiver(MainViewInterface mainViewInterface) {
        this.mainViewInterface = mainViewInterface;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if(Intent.ACTION_SCREEN_OFF.equals(intent.getAction()))
            mainViewInterface.moveFirstPage();
    }
}
