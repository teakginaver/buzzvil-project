package com.example.myapptest.views;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapptest.R;
import com.example.myapptest.utils.network.ApiResult;
import com.example.myapptest.utils.ProgressInterface;
import com.example.myapptest.utils.ProgressPopup;

public abstract class NetworkBaseActivity extends AppCompatActivity implements ApiResult, ProgressInterface {

    protected Context context;
    private ProgressPopup mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
    }


    @Override
    public void onResult(Object o) {}

    @Override
    public void onError(Throwable throwable) {}

    @Override
    public void startProgress() {
        new Handler(Looper.getMainLooper()).post(() -> {
            progressShow();
        });
    }

    @Override
    public void endProgress() {
        new Handler(Looper.getMainLooper()).post(() -> {
            progressDismiss();
        });
    }

    private void progressShow(){
        if (mProgress != null)
            if (mProgress.isShowing())
                mProgress.dismiss();

        mProgress = new ProgressPopup(this);
        mProgress.setCancelable(false);
        mProgress.show();
    }

    private void progressDismiss(){
        if(mProgress != null && mProgress.isShowing())
            mProgress.dismiss();
    }

    public void warningPopup(String msg){
        if(msg == null || isFinishing())return;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.popup_title_warning)).setMessage(msg).setNegativeButton("확인", (dialogInterface, i) -> dialogInterface.dismiss());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}