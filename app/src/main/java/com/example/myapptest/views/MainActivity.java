package com.example.myapptest.views;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.example.myapptest.R;
import com.example.myapptest.databinding.ActivityMainBinding;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.utils.localdb.LocalDBManager;
import com.example.myapptest.utils.localdb.LocalDBManagerImpl;
import com.example.myapptest.views.hiddenviews.MainContentsFragment;
import com.example.myapptest.views.hiddenviews.MainFavoritesFragment;
import com.example.myapptest.views.hiddenviews.FragmentResizer;
import com.example.myapptest.views.viewmodels.LocalDBViewModel;
import com.example.myapptest.views.viewpager.ArrowAnimation;
import com.example.myapptest.views.viewmodels.NetworkViewModel;
import com.example.myapptest.views.viewpager.MainViewPagerAdapter;
import com.example.myapptest.views.viewpager.PageItem;

import java.util.ArrayList;

public class MainActivity extends NetworkBaseActivity implements MainViewInterface {

    private ActivityMainBinding binding;
    private MainViewPagerAdapter adpter;
    private NetworkViewModel networkViewModel;
    private LocalDBViewModel localDBViewModel;
    private FragmentResizer fragmentResizer;

    private MainFavoritesFragment mainFavoritesFragment;
    private MainContentsFragment mainContentsFragment;
    private ScreenOnOffBroadcastReceiver screenOnOffBroadcastReceiver;

    //뷰페이지 변경 이벤트 처리
    private ViewPager2.OnPageChangeCallback callback = new ViewPager2.OnPageChangeCallback(){
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            localDBViewModel.viewCntpp(position);
            localDBViewModel.getWebUrl(position);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.Theme_Myapptest);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initObject();
        settingFragment();
        initLiveData();

        networkViewModel.connectApiData(this);
        localDBViewModel.getDBData();
    }

    private void initObject(){
        LocalDBManager localDBManager = new LocalDBManagerImpl(context);
        networkViewModel = new ViewModelProvider(this).get(NetworkViewModel.class);
        localDBViewModel = new ViewModelProvider(this).get(LocalDBViewModel.class);
        networkViewModel.init(localDBManager);
        localDBViewModel.init(localDBManager);
        fragmentResizer = new FragmentResizer(context);
        mainFavoritesFragment = new MainFavoritesFragment(this);
        mainContentsFragment = new MainContentsFragment(this);

        adpter = new MainViewPagerAdapter(this, new ArrayList<>());
        binding.mainViewpager.setAdapter(adpter);
        binding.mainViewpager.registerOnPageChangeCallback(callback);

        screenOnOffBroadcastReceiver = new ScreenOnOffBroadcastReceiver(this);
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        registerReceiver(screenOnOffBroadcastReceiver, intentFilter);
    }

    private void settingFragment(){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.favoritesLayout, mainFavoritesFragment);
        transaction.add(R.id.contentsLayout, mainContentsFragment);
        transaction.commit();
    }

    private void initLiveData(){
        localDBViewModel.pageItem.observe(this, pageItems ->{
            if(pageItems == null)pageItems = new ArrayList<>();
            if(adpter != null)adpter.setList(pageItems);
            if(pageItems.size() > 0)new ArrowAnimation(this).start(binding.leftArrow, binding.rightArrow);
        });

        networkViewModel.isNewNetworkData.observe(this, isData ->{
            if(isData){
                networkViewModel.setNewNetworkDataFlag(false);
                localDBViewModel.getDBData();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        moveFirstViewPagerOnStart();// 화면 off 후 첫페이지로 이동
        resizeFrameLayout();// 상세보기, 즐겨찾기 사이즈 조절
    }

    @Override
    protected void onStop() {
        super.onStop();
        hiddenFragment();// 상세보기 및 즐겨찾기 닫기.
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding.mainViewpager.unregisterOnPageChangeCallback(callback);
        unregisterReceiver(screenOnOffBroadcastReceiver);
    }

    /**
     * FrameLayout 전체 크기 조절 (상세보기, 즐겨찾치 프래그먼트)
     * */
    private void resizeFrameLayout(){
        binding.relativeLayout.setVisibility(View.INVISIBLE);
        fragmentResizer.makeHiddenLayout(binding.relativeLayout);
        binding.relativeLayout.setVisibility(View.VISIBLE);
    }


    private long MAXTIMER = 3000l;
    private long timer = 0l;
    @Override
    public void onBackPressed() {
        if(hiddenFragment())return;

        long cur = System.currentTimeMillis();
        if(cur > timer + MAXTIMER){
            timer = cur;
            Toast.makeText(context, getResources().getString(R.string.app_finish_message), Toast.LENGTH_LONG).show();
            return;
        }
        super.onBackPressed();
    }

    private boolean hiddenFragment(){
        //즐겨찾기 숨김
        if(binding.contentsLayout.getVisibility() == View.INVISIBLE){
            mainFavoritesFragment.hidden();
            return true;
        }
        //상세보기 숨김
        if(binding.favoritesLayout.getVisibility() == View.INVISIBLE){
            mainContentsFragment.hidden();
            return true;
        }
        return false;
    }



    /**
     * 화면 오프시 첫페이지로 이동 onStop
     * */
    @Override
    public void moveFirstPage() {
        //binding.mainViewpager.setCurrentItem(0); // onStop에서 잘 동작 안함.
        //다른 로직으로 변경
        localDBViewModel.setFirstPage(true);
        localDBViewModel.getDBData();
    }

    /**
     * 화면 오프시 첫페이지로 이동 onStart
     * */
    private void moveFirstViewPagerOnStart(){
        if(localDBViewModel.isFirstPage()){
            localDBViewModel.setFirstPage(false);
            binding.mainViewpager.setCurrentItem(0, false);
        }
    }

    /**
     * 프래그먼트 - 액티비티 간 인터페이스 구현
     * */
    @Override
    public FragmentResizer getFragmentResizer() { return fragmentResizer; }
    @Override
    public LocalDBViewModel getDBViewModel() { return localDBViewModel; }
    @Override
    public FrameLayout getFavoritesLayout() {return binding.favoritesLayout;}
    @Override
    public FrameLayout getContentsLayout() { return binding.contentsLayout; }


    /**
     * 캠페인 그만보기
     * */
    @Override
    public void stopContents() {
        hiddenFragment();
        localDBViewModel.deleteCampaign(binding.mainViewpager.getCurrentItem());
    }

    /**
     * 즐겨찾기 추가 해제
     * */
    @Override
    public void setFavorites(@NonNull ImageView v, PageItem item, boolean fromViewPager) {
        localDBViewModel.setFavorites(item);
        viewMessageAndSetImage(v, item, fromViewPager);
    }

    private void viewMessageAndSetImage(@NonNull ImageView v, @NonNull PageItem item, boolean fromViewPager){
        if(!fromViewPager)adpter.notifyDataSetChanged();

        if(item.getFavorites() == Favorites.N){
            v.setImageResource(R.mipmap.inactive_favorites);
        }else{
            v.setImageResource(R.mipmap.active_favorites);
            if(fromViewPager)Toast.makeText(this, getResources().getString(R.string.favorites_y), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 즐겨찾기 데이터
     * */
    @Override
    public void findFavoritesList() {
        localDBViewModel.findFavoritesList();
    }


    /**
     * pageView 이동
     * */
    @Override
    public void movePage(PageItem item) {
        int position = localDBViewModel.findPositionByPageItem(item);
        if(position < 0)return;
        hiddenFragment();
        binding.mainViewpager.setCurrentItem(position, true);
    }


    /**
     * 서버 데이터 받기 구현
     * */
    @Override
    public void onResult(Object o) {networkViewModel.onResult(o);}

    @Override
    public void onError(Throwable throwable) {warningPopup(getResources().getString(R.string.popup_body_warning));}


}