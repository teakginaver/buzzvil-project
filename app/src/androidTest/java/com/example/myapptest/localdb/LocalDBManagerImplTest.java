package com.example.myapptest.localdb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.myapptest.utils.CampaignsSorter;
import com.example.myapptest.utils.Category;
import com.example.myapptest.utils.Favorites;
import com.example.myapptest.utils.localdb.LocalDBManager;
import com.example.myapptest.utils.localdb.LocalDBManagerImpl;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.views.viewpager.PageItem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import util.Log;

@RunWith(AndroidJUnit4.class)
public class LocalDBManagerImplTest {
    LocalDBManager localDBManager;
    @Before
    public void setUp() throws Exception {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        localDBManager = new LocalDBManagerImpl(appContext);
        settingConfig();
        deleteEntry();
        makeEntry();
    }

    @After
    public void tearDown() throws Exception {
        deleteEntry();
        localDBManager.close();
    }

    private void settingConfig(){
        final String ratio = "3:1";
        ConfigEntity config = new ConfigEntity(ratio);
        localDBManager.insertOrUpdate(config);
    }

    private void makeEntry(){
        List<CampaignsEntity> list = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            CampaignsEntity e = new CampaignsEntity();
            e.setId(100 + i);
            e.setName("Test");
            e.setImageUrl("https://s3-ap-northeast-1.amazonaws.com/buzzvi.test/creatives/ad_creative_1.jpg");
            e.setFirstDisplayPriority(i);
            e.setFirstDisplayWeight(100 + i);
            e.setFrequency(100);
            e.setLanding_url("http://naver.com");
            e.setView_cnt(0);
            e.setCategory(Category.AD);
            if(i >= 5)e.setCategory(Category.CONTENTS);
            e.setFavorites(Favorites.N);
            list.add(e);
        }
        localDBManager.insertOrUpdate(list);
    }

    private void deleteEntry(){
        for(int i = 0; i < 10; i++){
            localDBManager.deleteById(100 + i);
        }
    }

    //////////////////////Config Test ///////////////////////////////
    @Test
    public void getConfigTest(){
        ConfigEntity config = localDBManager.findConfig();
        assertNotNull(config);
    }

    @Test
    public void updateConfigTest(){
        final int cnt = 3;
        ConfigEntity config = localDBManager.findConfig();
        config.setFirstViewCnt(cnt);

        // cnt == 3
        localDBManager.update(config);
        config = localDBManager.findConfig();
        assertEquals(cnt, config.getFirstViewCnt());

        // cnt == 4
        localDBManager.updateConfigViewCnt();
        config = localDBManager.findConfig();
        assertEquals(cnt + 1, config.getFirstViewCnt());
    }

    // 광고와 컨텐츠 비율 검증
    @Test
    public void configRatioTest() throws Exception{
        final String ratio = "3:1";
        ConfigEntity config = localDBManager.findConfig();
        assertEquals(ratio, config.getFirstAdRatio());
        assertEquals(3, config.getAdCnt());
        assertEquals(1, config.getContentsCnt());
        assertEquals(4, config.allCnt());
    }


    //////////////////////Campaigns Test ///////////////////////////////

    // 캠페인 가져오기
    @Test
    public void getAllCampaigns() throws Exception{
        List<PageItem> allCampaigns = localDBManager.getAllCampaigns();
        assertTrue(0 < allCampaigns.size());
    }


    // 캠페인 ViewCnt
    @Test
    public void viewCntTest() throws Exception{
        List<CampaignsEntity> allCampaigns = localDBManager.findCampaignsList();
        CampaignsEntity e = allCampaigns.get(0);

        localDBManager.updateViewCnt(e.getView_cnt() + 1, e.getId());

        allCampaigns = localDBManager.findCampaignsList();
        CampaignsEntity item = null;
        for (CampaignsEntity allCampaign : allCampaigns) {
            if(allCampaign.getId() == e.getId()){
                item = allCampaign;
                break;
            }
        }
        assertEquals(item.getView_cnt(), e.getView_cnt() + 1);

    }

    // 캠페인 favorites
    @Test
    public void favoritesTest() throws Exception{
        List<CampaignsEntity> allCampaigns = localDBManager.findCampaignsList();
        CampaignsEntity e = allCampaigns.get(0);

        localDBManager.updateFavorites(Favorites.Y, e.getId());

        allCampaigns = localDBManager.findCampaignsList();
        CampaignsEntity item = null;
        for (CampaignsEntity allCampaign : allCampaigns) {
            if(allCampaign.getId() == e.getId()){
                item = allCampaign;
                break;
            }
        }

        assertEquals(item.getFavorites(), Favorites.Y);

        localDBManager.deleteById(e.getId());
    }


}