package com.example.myapptest.utils;

import static org.junit.Assert.*;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import com.example.myapptest.utils.localdb.LocalDBManager;
import com.example.myapptest.utils.localdb.LocalDBManagerImpl;
import com.example.myapptest.utils.localdb.entity.CampaignsEntity;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.views.viewpager.PageItem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CampaignsSorterTest {
    List<CampaignsEntity> allList;
    ConfigEntity config;
    @Before
    public void setUp() throws Exception {
        settingConfig();
        makeEntry();
    }

    @After
    public void tearDown() throws Exception {
        allList = null;
        config = null;
    }
    private void settingConfig(){
        final String ratio = "3:1";
        config = new ConfigEntity(ratio);
    }

    private void makeEntry(){
        allList = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            CampaignsEntity e = new CampaignsEntity();
            e.setId(100 + i);
            e.setName("Test");
            e.setImageUrl("https://s3-ap-northeast-1.amazonaws.com/buzzvi.test/creatives/ad_creative_1.jpg");
            e.setFirstDisplayPriority(i);
            e.setFirstDisplayWeight(100 + i);
            e.setFrequency(100);
            e.setLanding_url("http://naver.com");
            e.setView_cnt(0);
            e.setCategory(Category.AD);
            if(i >= 5)e.setCategory(Category.CONTENTS);
            e.setFavorites(Favorites.N);
            allList.add(e);
        }
    }

    ///////////////////////////////

    @Test
    public void getADFirstPage(){
        CampaignsSorter campaignsSorter = new CampaignsSorter(config, allList);
        List<PageItem> start = campaignsSorter.start();
        assertEquals(100 , start.get(0).getId());
    }


    @Test
    public void getContentsFirstPage(){
        config.setFirstViewCnt(3);
        CampaignsSorter campaignsSorter = new CampaignsSorter(config, allList);
        List<PageItem> start = campaignsSorter.start();
        assertEquals(105 , start.get(0).getId());
    }
}