package com.example.myapptest.utils.network;

import com.example.myapptest.utils.network.request.ConfigApiRequest;
import com.example.myapptest.utils.network.response.AdApiResponse;
import com.example.myapptest.utils.network.response.ConfigApiResponse;
import com.example.myapptest.utils.localdb.entity.ConfigEntity;
import com.example.myapptest.utils.network.response.ContentsApiResponse;

import static org.junit.Assert.*;

import org.junit.Test;

public class ApiFactoryTest{

    @Test
    public void requestTest(){
        try{
            Result result = new Result();
            new ApiFactory(result, ApiFactory.makeRequest(new ConfigApiRequest("getConfig")
                    , new ConfigApiRequest("getContentsList")
                    , new ConfigApiRequest("getAdList"))).newThread();


            for(int i = 0;i < 10; i++){
                Thread.sleep(1000);
                if(result.o1 != null && result.o2 != null || result.o3 != null){
                    assertEquals(1, 1);
                    return;
                }
            }
            assertEquals(1, 2);
        }catch (Exception e){
            assertFalse("ApiFactory connection Error" ,true);
        }

    }

    public class Result implements ApiResult {
        public ConfigApiResponse o1;
        public ContentsApiResponse o2;
        public AdApiResponse o3;
        @Override
        public void onResult(Object o) {

            if(o instanceof ConfigApiResponse){o1 = (ConfigApiResponse)o;}
            if(o instanceof ContentsApiResponse){o2 = (ContentsApiResponse)o;}
            if(o instanceof AdApiResponse){o3 = (AdApiResponse)o;}

        }

        @Override
        public void onError(Throwable throwable) {}
    }
}